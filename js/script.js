function Verify_Items() {
    // console.log('verifying...')
    let items = document.getElementsByTagName('li')
    for (let i = 0; i < items.length; i++) {
        items[i].addEventListener('click', Remove_Item)
    }
}

var Remove_Item = (event) => {
    if (document.getElementsByTagName('li').length == 1) {
        event.target.remove()
        document.getElementById('item-list').classList.remove('w3-border')
    } else {
        event.target.remove()
    }
}

var Add_Item = () => {
    let value = document.getElementById('item-input').value
    if (value) {
        if (document.getElementsByTagName('li').length == 0) {
            document.getElementById('item-list').classList.add('w3-border')
            let li = document.createElement('li')
            li.className = 'w3-hover-gray'
            li.appendChild(document.createTextNode(value))
            document.getElementById('item-list').appendChild(li)
            Verify_Items()
            document.getElementById('item-input').value = ''
        } else {
            let li = document.createElement('li')
            li.className = 'w3-hover-gray'
            li.appendChild(document.createTextNode(value))
            document.getElementById('item-list').appendChild(li)
            Verify_Items()
            document.getElementById('item-input').value = ''
        }
    } else {
        alert('Please enter a value.')
    }
}

document.querySelector('html').addEventListener('load', Verify_Items())
document.getElementById('add-button').addEventListener('click', Add_Item)
document.getElementById('btn').addEventListener('click', () => {
    console.log(event)
    console.log(event.target)
})
